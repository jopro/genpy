# attempts to use genetic algorithms to find a solution to a simple problem
from gen import *
from random import randrange

def value(dna):
        return int("".join(dna))

def fitnessfunc(algo, dna):
	curr = value(dna)
        nearest = int(curr**0.5)**2.0
        if nearest == 0:
            return 1
        f = abs(curr-nearest)/nearest
	return f

def main():
	options = map(str, range(0, 5))

	algo = Evolution(options, 0.3, 10, 4)

	algo.fitness = lambda genome: fitnessfunc(algo, genome)
        s = algo.solve()
        print(s)
        print(value(s))

if __name__ == "__main__":
	main()
