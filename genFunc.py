# attempts to use genetic algorithms to find a solution to a simple problem
from gen import *
from random import randrange

def value(dna, x):
	try:
		string = "".join(dna)
		string.replace('x', str(x))
		return float(eval(string))
	except:
		return None

def fitnessfunc(algo, dna):
	f = 0
        n = 50
	for i in range(n):
		x = randrange(-100, 100)
		curr = value(dna, x)
                
		target = 3*x+4

		if curr != target:
			f += 1
	return f/float(n)

def main():
	options = ['+4', '+3', '+2', '-x', '*', '+x']

	algo = Evolution(options, fitnessfunc)
        print(algo.solve())
        return

if __name__ == "__main__":
	main()
