# attempts to use genetic algorithms to find a solution to a simple problem
from gen import *
from random import randrange

def value(dna):
	try:
		return float(eval("".join(dna)))
	except:
		return 0

def fitnessfunc(algo, dna):
	target = 9001
	curr = value(dna)
	
	f = abs(curr/target-1)
	return f

def main():
	options = ['0', '+1', '-1']

	algo = Evolution(options)
	algo.fitness = lambda genome: fitnessfunc(algo, genome)
        s = algo.solve()
        print(s)
        print(value(s))

if __name__ == "__main__":
	main()
