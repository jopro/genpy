# attempts to use genetic algorithms to find a solution to a simple problem
from gen import *
from random import randrange

def value(dna, x):
	try:
		string = "".join(dna)
		string.replace('x', str(x))
		return float(eval(string))
	except:
		pass
        return 100000

def fitnessfunc(algo, dna):
	f = 0
        n = 50
	for i in range(n):
		x = (random()-0.5)*10000
		curr = value(dna, x)
                
		target = x*x-x+1

                v = abs(curr-target)/(1.0*target)
                f += v

        if f != 0:
            f += len(dna)/10.0

	return f/float(n)

def main():
	options = ['+4', '+3', '+2', '+1', '+x', '-x', '*']

	algo = Evolution(options, fitnessfunc)
        print(algo.solve())
        return

if __name__ == "__main__":
	main()
