# attempts to use genetic algorithms to find a solution to a simple problem
from gen import *
from random import randrange

def value(dna):
        return int("".join(dna))

def fitnessfunc(algo, dna):
	curr = value(dna)
        
        if curr < 2:
            return 1

        f = 0
        i = 2
        while i < curr**0.5+1:
            if curr % i == 0:
                f += 1
            i+=1

        d = int(curr**0.5)-1.0

        if d != 0:
            f = f/d
        else:
            f = 1

        return f

def main():
	options = map(str, range(0, 9))

	algo = Evolution(options, fitnessfunc)
        s = algo.solve()
        print(s)
        print(value(s))

if __name__ == "__main__":
	main()
