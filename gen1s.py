# attempts to use genetic algorithms to find a solution to a simple problem
from gen import *
from random import randrange

def value(dna):
	try:
		return dna.count("1") 
	except:
		return 0

def fitnessfunc(algo, dna):
	target = 20.0
	curr = value(dna)
	
	f = abs(curr/target-1)
	return f

def main():
	options = ['0', '1']

	algo = Evolution(options, 0.05, 100, 20)

	algo.fitness = lambda genome: fitnessfunc(algo, genome)

        print algo.solve()

        return
	while(raw_input("") == ""):	
		algo.nextGeneration()
		best = algo.getBest()
		print(best)
		print(value(best.dna))
		print(str((1-fitnessfunc(algo, best.dna))*100)+"%")
	#for genome in algo.currentGen:
	#	print(genome)

if __name__ == "__main__":
	main()
