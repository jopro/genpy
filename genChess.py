# attempts to use genetic algorithms to find a solution to a simple problem
from gen import *
from random import randrange

def value(dna):
		numConflicts = 0
		otherXs = []
		otherYs = []
		otherXYs = []
		otherYXs = []
		for index in range(0, len(dna)-1,2):
			if dna[index] in otherXs:
				numConflicts += 1

			if dna[index+1] in otherYs:
				numConflicts += 1

			if int(dna[index])-int(dna[index+1]) in otherXYs:
				numConflicts += 1

			if 8-int(dna[index+1])-int(dna[index]) in otherYXs:
				numConflicts += 1
	
			otherXs.append(dna[index])
			otherYs.append(dna[index+1])
			otherXYs.append(int(dna[index])-int(dna[index+1]))
			otherYXs.append(8-int(dna[index+1])-int(dna[index]))
		return numConflicts/7.0

def draw(dna):
	grid = range(8)
	for i in grid:
		grid[i] = range(8)
		for j in grid[i]:
			grid[i][j] = " "
	
	for index in range(0, len(dna)-1, 2):
		x = int(dna[index])-1
		y = int(dna[index+1])-1
		grid[x][y] = "x"

	print("\n".join(map(lambda line: "".join(line), grid)))

def fitnessfunc(algo, dna):
	curr = value(dna)
	f = curr+abs(len(dna)-16)
        return f

def main():
	options = ['1', '2','3', '4', '5', '6', '7', '8']

	algo = Evolution(options, fitnessfunc)
        out = algo.solve()
	print(out)
        draw(out)
        return None

if __name__ == "__main__":
	main()
