# attempts to use genetic algorithms to find a solution to a simple problem
from gen import *

def fitnessfunc(algo, dna):
	pattern = ['G','A','T','T','A','C','A']
	
	total = 0
	for i in range(len(pattern)):
		if i < len(dna) and pattern[i] == dna[i]:
			total += 1

        total -= abs(len(pattern) - len(dna))

	return 1-total/float(len(pattern))

def main():
	options = ['G', 'C', 'A', 'T', '.']
	algo = Evolution(options, fitnessfunc)
        print(algo.solve())

if __name__ == "__main__":
	main()
