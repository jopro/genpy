#provides a very simple framework for performing genetic generation of algorithms
from random import choice, random

class Genome:
    def __init__(self, options, length, mutation, parents=[]):
        if len(parents) > 0:
            self.dna = range(len(parents[0]))
        else:
            self.dna = range(length)
        
        for i in self.dna:
            o = choice(options)
            if len(parents) > 0 and random() > mutation:
                p = choice(parents)
                if i < len(p):
                    o = p[i]#take this dna string from the parents
            self.dna[i] = o

        if random() < mutation:
            index = choice(range(len(self.dna)))
            if random() > 0.5 or len(self.dna) <= 1:
                self.dna.insert(index, choice(options))
            else:
                self.dna.pop(index)

class Evolution():
    def __init__(self, options, fitness):
        self.options = options
        self.mutationP = 0.5
        self.populationSize = 1000
        self.genomeLength = 5
        self.generation = 0
        self.fitness = lambda genome: fitness(self, genome)

        self.currentGen = []
        for i in range(self.populationSize):
            genome = Genome(options, self.genomeLength, self.mutationP)
            self.currentGen.append(genome)
        self.getBest() 

    def fitness(self, dna):#returns 0..1
        return 1#evaluate the fitness of the  genome

    def getBest(self):
        self.currentGen = sorted(self.currentGen, key=lambda genome: self.fitness(genome.dna))
        return self.currentGen[0]

    def solve(self):
        solved = 1
        while solved > 0:
            self.nextGeneration()
            first = self.currentGen[0].dna
            solved = self.fitness(first)
            print str(self.generation)+":"+str(1-solved)
            print first
        return first
        
    def nextGeneration(self):
        self.generation+=1
        best = self.currentGen[0:len(self.currentGen)/4]
        #make new genomes
        newGenomes = range(self.populationSize-len(best))
        for gen in newGenomes:
            parents = [choice(best).dna, choice(best).dna]
            if int(random()*100) == 0 or parents[0] == parents[1]:
                parents = []
            genome = Genome(self.options, self.genomeLength, self.mutationP, parents)
            newGenomes[gen] = genome
        newGenomes.extend(best)
        self.currentGen = newGenomes #update the generation
        self.getBest()#sort by fitness
